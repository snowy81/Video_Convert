$SearchPath = "Path-to-folder"

Get-ChildItem $SearchPath\* -Include *.avi, *.mkv, *.mp4* -Recurse;

Set-Location -Path 'F:\scripts\ffmpeg\bin';

foreach ($OldVideo in $oldVideos)
{

$Ext = [System.IO.Path]::GetExtension("$OldVideo")

If($Ext -eq ".avi")
{
$newVideo = [io.path]::ChangeExtension($OldVideo.FullName, '.mp4')
& "F:\scripts\ffmpeg\bin\ffmpeg.exe" -i $($OldVideo) -ac 2 $($NewVideo)
}

elseif($Ext -eq ".mkv")
{
$newVideo = [io.path]::ChangeExtension($OldVideo.FullName, '.mp4')
& "F:\scripts\ffmpeg\bin\ffmpeg.exe" -i $($OldVideo) -c:v copy -acodec libmp3lame -ab 160k -ar 48000 -async 48000 -ac 2 $($NewVideo)
}

elseif($Ext -eq ".mp4")
{
$channels = & "F:\scripts\ffmpeg\bin\ffprobe.exe" -loglevel error -select_streams a:0 -show_entries stream=channels -of default=nw=1:nk=1 $OldVideo
  If($channels -ne 2)
  {
  $NewVideo = ([System.IO.Path]::GetFileNameWithoutExtension("$OldVideo")) + ".remix.mp4"
  & "F:\scripts\ffmpeg\bin\ffmpeg.exe" -i $($OldVideo) -ac 2 $($NewVideo)
  }
}

}
Set-Location -Path 'H:\Downloads\Work';